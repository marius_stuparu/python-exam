import sqlite3
import os
from datetime import datetime
import smtplib
import secrets

class StocMagazin:
    """Baza de date de stocuri pentru un magazin"""

    def __init__(self, numeMagazin):
        """Initializare baza de date"""

        self.__dbConnection = None
        self.__numeMagazin = numeMagazin

        cursor = None

        try:
            cursor = self.__dbOpen()

            if os.environ.get('ENV') == 'DEVELOPMENT':
                # Pornire de la zero in mod development
                sql_drop_table_produse = """DROP TABLE IF EXISTS produse"""
                sql_drop_table_operatiuni = """DROP TABLE IF EXISTS operatiuni"""
                cursor.execute(sql_drop_table_produse)
                cursor.execute(sql_drop_table_operatiuni)

            sql_create_table_produse = """
                CREATE TABLE IF NOT EXISTS produse(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    nume_produs VARCHAR(20) NOT NULL UNIQUE,
                    categorie VARCHAR(20) NOT NULL,
                    cantitate FLOAT DEFAULT 0,
                    stoc_minim FLOAT DEFAULT 0,
                    unitate_masura VARCHAR(10) NOT NULL,
                    pret_achizitie FLOAT DEFAULT 0,
                    pret_vanzare FLOAT DEFAULT 0
                );"""
            sql_create_table_io = """
                CREATE TABLE IF NOT EXISTS operatiuni(
                    id INTEGER PRIMARY KEY AUTOINCREMENT,
                    id_produs INTEGER,
                    data_operatiune TIMESTAMP,
                    cantitate_intrata FLOAT DEFAULT 0,
                    pret_achizitie FLOAT DEFAULT 0,
                    cantitate_iesita FLOAT DEFAULT 0,
                    pret_vanzare FLOAT DEFAULT 0,
                    FOREIGN KEY (id_produs) REFERENCES produse(id)
                );"""
            cursor.execute(sql_create_table_produse)
            cursor.execute(sql_create_table_io)
        except sqlite3.Error as error:
            print("Failed to set-up sqlite database", error)
        finally:
            if cursor:
                self.__dbClose(cursor)

    def __dbOpen(self):
        """Deschide conexiunea cu baza de date"""

        cursor = None

        try:
            self.__dbConnection = sqlite3.connect(self.__numeMagazin + '.db')
            cursor = self.__dbConnection.cursor()
        except sqlite3.Error as error:
            print("Failed to connect to sqlite database", error)
        finally:
            if self.__dbConnection:
                return cursor

    def __dbClose(self, cursor, cancel = False):
        """Inchide conexiunea cu baza de date"""

        try:
            if not cancel:
                self.__dbConnection.commit()

            cursor.close()
            self.__dbConnection.close()
        except sqlite3.Error as error:
            print("Failed to connect to sqlite database", error)
        finally:
            return True

    def __getProduct(self, nume_produs):
        """Gaseste id-ul produsului in baza de date"""

        produs = None

        try:
            dbCursor = self.__dbOpen()
            sql_get_product_id = f"""
                SELECT * FROM produse WHERE nume_produs = "{nume_produs}"
            """
            dbCursor.execute(sql_get_product_id)
            produs = dbCursor.fetchone()
        except sqlite3.Error as error:
            print("Failed to execute sqlite search", error)
        finally:
            if produs:
                return {
                    'id': produs[0],
                    'nume_produs': produs[1],
                    'categorie': produs[2],
                    'cantitate': produs[3],
                    'stoc_minim': produs[4],
                    'unitate_masura': produs[5],
                    'pret_achizitie': produs[6],
                    'pret_vanzare': produs[7]
                }
            else: return None

    def __getProductAverageBuyPrice(self, id_produs):
        """Aduce toate preturile de achizitie din baza de date si calculeaza media"""

        media_ponderata = None

        try:
            dbCursor = self.__dbOpen()
            sql_query_get_buy_prices = f"""
                SELECT pret_achizitie, cantitate_intrata FROM operatiuni
                WHERE id_produs = "{id_produs}"
            """
            dbCursor.execute(sql_query_get_buy_prices)
            preturi = dbCursor.fetchall()
            suma = 0
            ponderi = 0
            for pret, cantitate in preturi:
                suma += pret * cantitate
                ponderi += cantitate
            media_ponderata = suma / ponderi
        except sqlite3.Error as error: print("Failed to insert into sqlite database", error)
        finally:
            return media_ponderata

    def adaugareProdus(self, nume_produs, categorie, stoc_minim, um = 'kg', cantitate = 0):
        """Adauga un produs in baza de date"""

        try:
            foundProduct = self.__getProduct(nume_produs)

            if not foundProduct:
                dbCursor = self.__dbOpen()
                sql_add_product = f"""
                    INSERT INTO produse (nume_produs, categorie, cantitate, stoc_minim, unitate_masura)
                    VALUES ("{nume_produs}", "{categorie}", "{cantitate}", "{stoc_minim}", "{um}")
                """
                dbCursor.execute(sql_add_product)
                self.__dbClose(dbCursor)
            else:
                print(f"Produsul {nume_produs} exista deja in baza de date")
        except sqlite3.Error as error:
            print("Failed to insert into sqlite database", error)

    def actualizarePret(self, nume_produs):
        """Actualizare preturi produse"""

        dbCursor = None
        foundProduct = self.__getProduct(nume_produs)

        try:
            if foundProduct:
                dbCursor = self.__dbOpen()
                pret_achizitie = float(self.__getProductAverageBuyPrice(foundProduct['id']))
                adaos_comercial = float(os.environ.get('ADAOS_COMERCIAL'))
                pret_vanzare = pret_achizitie * (1 + (adaos_comercial / 100))
                #  Actualizeaza cantitatea totala a produsului
                sql_update_quantity = f"""
                    UPDATE produse SET
                    pret_achizitie = "{str(round(pret_achizitie, 2))}",
                    pret_vanzare = "{str(round(pret_vanzare, 2))}"
                    WHERE produse.id = {foundProduct['id']}
                """
                dbCursor.executescript(sql_update_quantity)
        except sqlite3.Error as error:
            print("Failed to update sqlite database", error)
        finally:
            if dbCursor:
                self.__dbClose(dbCursor)

    def intrareProdus(self, nume_produs, cantitate, pret_achizitie, data = datetime.now()):
        """Adauga o operatiune de intrare cantitate in baza de date"""

        dbCursor = None
        foundProduct = self.__getProduct(nume_produs)

        try:
            if foundProduct:
                dbCursor = self.__dbOpen()
                # Adauga intrarea in tabela de operatiuni
                sql_add_io_entry = f"""
                    INSERT INTO operatiuni (id_produs, data_operatiune, cantitate_intrata, pret_achizitie)
                    VALUES ("{foundProduct['id']}", "{data}", "{cantitate}", "{pret_achizitie}")
                """
                dbCursor.execute(sql_add_io_entry)

                #  Actualizeaza cantitatea totala a produsului
                cantitate_noua = float(foundProduct['cantitate']) + cantitate
                sql_update_quantity = f"""
                    UPDATE produse SET cantitate = "{round(cantitate_noua, 2)}"
                    WHERE id = {foundProduct['id']}
                """
                dbCursor.execute(sql_update_quantity)
            else:
                raise NameError(f"Produsul {nume_produs} nu se afla in baza de date")
        except sqlite3.Error as error: print("Failed to insert into sqlite database", error)
        except NameError as error: print(error)
        finally:
            if dbCursor:
                self.__dbClose(dbCursor)
            self.actualizarePret(nume_produs)

    def iesireProdus(self, nume_produs, cantitate, data = datetime.now()):
        """Adauga o operatiune de iesire cantitate in baza de date"""

        dbCursor = None

        try:
            foundProduct = self.__getProduct(nume_produs)

            if foundProduct['cantitate'] - cantitate < 0:
                raise PermissionError(f"""
                    Cantitatea de iesire ({cantitate} {foundProduct['unitate_masura']}) este mai mare decat stocul 
                    produsului {nume_produs} ({foundProduct['cantitate']} {foundProduct['unitate_masura']})
                """)

            if foundProduct:
                dbCursor = self.__dbOpen()
                # Adauga intrarea in tabela de operatiuni
                sql_add_io_entry = f"""
                    INSERT INTO operatiuni (id_produs, data_operatiune, cantitate_iesita, pret_vanzare)
                    VALUES ("{foundProduct['id']}", "{data}", "{cantitate}", "{foundProduct['pret_vanzare']}")
                """
                dbCursor.executescript(sql_add_io_entry)

                #  Actualizeaza cantitatea totala a produsului
                cantitate_noua = float(foundProduct['cantitate']) - cantitate
                sql_update_quantity = f"""
                   UPDATE produse SET cantitate = "{round(cantitate_noua, 2)}"
                   WHERE id = {foundProduct['id']}
               """
                dbCursor.executescript(sql_update_quantity)
            else:
                raise NameError(f"Produsul {nume_produs} nu se afla in baza de date")
        except sqlite3.Error as error: print("Failed to insert into sqlite database", error)
        except NameError as error: print(error)
        except PermissionError as error: print(error)
        finally:
            self.verificareStoc(nume_produs)
            if dbCursor:
                self.__dbClose(dbCursor)

    def verificareStoc(self, nume_produs):
        """Verificare stoc dupa fiecare vanzare si emitere alerte in caz de scadere sub limita"""

        foundProduct = self.__getProduct(nume_produs)

        if foundProduct['cantitate'] < foundProduct['stoc_minim']:
            mailFrom = os.environ.get('EMAIL_ALERTE')
            mailTo = os.environ.get('EMAIL_ALERTE')
            mailSubject = """Stocul pentru {0} a scazut sub limita de {1}"""\
                .format(foundProduct['nume_produs'], foundProduct['stoc_minim'])
            mailBody = """Subject: {1}\r\n
            Atentie!\r\n
            \r\n
            Stocul pentru produsul {2} a scazut sub limita de minima de {3} {4}!
            """.format(os.environ.get('EMAIL_ALERTE'), mailSubject,
                       foundProduct['nume_produs'], foundProduct['stoc_minim'], foundProduct['unitate_masura'])

            try:
                mail_object = smtplib.SMTP(secrets.smtp_hostname, secrets.smtp_port)
                mail_object.ehlo()
                mail_object.login(secrets.smtp_user, secrets.smtp_password)
                mail_object.sendmail(mailFrom, mailTo, mailBody)
            except smtplib.SMTPException as err:
                print('Mesajul email nu a putut fi trimis', err)
            else:
                mail_object.close()
            finally:
                print('Alerta email expediata cu succes')
        else:
            return

if __name__ == '__main__':
    os.environ.update({'ENV': 'DEVELOPMENT'})
    os.environ.update({'ADAOS_COMERCIAL': '25'})
    os.environ.update({'EMAIL_ALERTE': 'marius@mariusstuparu.com'})

    # Definire magazin si produse
    dedemanPallady = StocMagazin('DedemanPallady')

    dedemanPallady.adaugareProdus('Parchet Stejar 12mm', 'parchet', 20, 'cutii')
    dedemanPallady.adaugareProdus('Plinta Parchet', 'plinta', 30, 'metri liniari')
    dedemanPallady.adaugareProdus('Chit Ceresit', 'chit', 10, 'saci')

    # Jurnal operatiuni
    dedemanPallady.intrareProdus('Plinta Parchet', 80.5, 18, datetime.fromisoformat('2019-11-15T14:00:00'))
    dedemanPallady.intrareProdus('Chit Ceresit', 20, 22.5, datetime.fromisoformat('2019-11-20T15:35:00'))

    """Incercare de vanzare invalida"""
    dedemanPallady.iesireProdus('Chit Ceresit', 25, datetime.fromisoformat('2019-11-20T16:20:00'))

    """Vanzare ce va declansa alerta de stoc minim"""
    dedemanPallady.iesireProdus('Chit Ceresit', 11, datetime.fromisoformat('2019-11-20T16:20:00'))

    dedemanPallady.intrareProdus('Plinta Parchet', 110, 17.2, datetime.fromisoformat('2019-11-22T08:15:00'))
    dedemanPallady.intrareProdus('Parchet Stejar 12mm', 80, 38.7, datetime.fromisoformat('2019-11-30T10:20:33'))
    dedemanPallady.iesireProdus('Parchet Stejar 12mm', 10, datetime.fromisoformat('2019-12-02T11:45:00'))
    dedemanPallady.iesireProdus('Plinta Parchet', 22.7, datetime.fromisoformat('2019-02-01T11:45:00'))
    dedemanPallady.intrareProdus('Parchet Stejar 12mm', 100, 35.2, datetime.fromisoformat('2019-12-03T12:11:00'))
    dedemanPallady.intrareProdus('Plinta Parchet', 70, 18.2, datetime.fromisoformat('2019-12-13T15:35:00'))
    dedemanPallady.intrareProdus('Chit Ceresit', 25, 21.25, datetime.fromisoformat('2020-01-04T09:25:00'))
    dedemanPallady.iesireProdus('Chit Ceresit', 20, datetime.fromisoformat('2020-01-05T16:20:00'))
    dedemanPallady.intrareProdus('Parchet Stejar 12mm', 50, 44.0, datetime.fromisoformat('2020-01-07T17:20:00'))
    dedemanPallady.intrareProdus('Chit Ceresit', 10, 22, datetime.fromisoformat('2020-01-16T15:35:00'))
